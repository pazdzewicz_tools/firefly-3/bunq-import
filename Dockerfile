FROM registry.gitlab.com/pazdzewicz_docker/phalcon-php/7.2:latest

WORKDIR /var/www

COPY ./ /var/www
COPY ./webserver/phalcon.conf /etc/nginx/conf.d/phalcon.conf

RUN apt update -y && \
    apt install -y \
                jq && \
    composer install