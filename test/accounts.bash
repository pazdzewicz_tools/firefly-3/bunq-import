curl -X 'POST' \
  "${FIREFLY_API_URL}/api/v1/accounts" \
  -H 'accept: application/vnd.api+json' \
  -H "Authorization: Bearer ${FIREFLY_API_KEY}" \
  -H 'Content-Type: application/json' \
  -d '{
  "name": "My checking account",
  "type": "asset",
}'