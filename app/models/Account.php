<?php

namespace Pazdzewicz\Models;

use Pazdzewicz\Firefly\Client as FireflyClient;

class Account extends \Phalcon\Mvc\Model
{
    protected $id;
    public function getId()
    {
        return $this->id;
    }

    protected $iban;
    public function getIban()
    {
        return $this->iban;
    }

    protected $owner;
    public function getOwner()
    {
        return $this->owner;
    }

    protected $name;
    public function getName()
    {
        return $this->name;
    }

    protected $currency;
    public function getCurrency()
    {
        return $this->currency;
    }

    protected $status;
    public function getStatus()
    {
        return $this->status;
    }

    protected $firefly_id;
    public function getFireflyId()
    {
        return $this->firefly_id;
    }

    protected $createdAt;
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    protected $modifiedAt;
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    protected $type;
    public function getType()
    {
        return $this->type;
    }

    protected $opening_balance;
    public function getOpeningBalance()
    {
        return $this->opening_balance;
    }

    protected $opening_balance_date;
    public function getOpeningBalanceDate()
    {
        return $this->opening_balance_date;
    }

    public function beforeCreate()
    {
        $this->createdAt = time();
        if(!$this->getFireflyId())
        {
            if(!$this->createFireflyAccount())
            {
                return false;
            }
        }
    }

    public function beforeSave()
    {
        if($this->createdAt == null)
        {
            $this->createdAt = time();
        }
        $this->modifiedAt = time();
        if($this->getFireflyId())
        {
            if(!$this->saveFireflyAccount())
            {
                return false;
            }
        }
    }

    private function createFireflyAccount()
    {
        $client = new FireflyClient();
        $data = array(
            'name' => $this->getName(),
            'type' => 'asset',
            'account_role' => 'defaultAsset',
            'include_net_worth' => true,
            'opening_balance' => $this->getOpeningBalance(),
            'opening_balance_date' => date("Y-m-d\TH:i:sP",$this->getOpeningBalanceDate()),
            'currency_code' => $this->getCurrency(),
            'iban' => $this->getIban(),
            'notes' => 'Owner: '.$this->getOwner()
        );

        switch ($this->getType())
        {
            case 'savings':
                $data['account_role'] = 'savingAsset';
            break;

            case 'joint':
                $data['account_role'] = 'sharedAsset';
            break;
            
            case 'single':
            default:
                $data['account_role'] = 'defaultAsset';
            break;
        }
        $request = $client->post('v1/accounts',$data);

        if(!$request)
        {
            return false;
        }

        #var_dump($request['data']['id']);

        if(isset($request['data']))
        {
            $this->firefly_id = $request['data']['id'];
        }
        return true;
    }

    private function saveFireflyAccount()
    {
        $client = new FireflyClient();
        $data = array(
            'name' => $this->getName(),
            'type' => 'asset',
            'account_role' => 'defaultAsset',
            'include_net_worth' => true,
            'currency_code' => $this->getCurrency(),
            'iban' => $this->getIban(),
            'notes' => 'Owner: '.$this->getOwner()
        );

        switch ($this->getType())
        {
            case 'savings':
                $data['account_role'] = 'savingAsset';
            break;

            case 'joint':
                $data['account_role'] = 'sharedAsset';
            break;
            
            case 'single':
            default:
                $data['account_role'] = 'defaultAsset';
            break;
        }
        $request = $client->put('v1/accounts/'.$this->getFireflyId(),$data);

        if(!$request)
        {
            return false;
        }

        #var_dump($request['data']['id']);

        #if(isset($request['data']))
        #{
        #    $this->firefly_id = $request['data']['id'];
        #}
        return true;
    }
}