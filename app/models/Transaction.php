<?php

namespace Pazdzewicz\Models;

use Pazdzewicz\Models\Account;

use Pazdzewicz\Firefly\Client as FireflyClient;

class Transaction extends \Phalcon\Mvc\Model
{
    protected $id;
    public function getId()
    {
        return $this->id;
    }

    protected $accountId;
    public function getAccount()
    {
        return Account::findFirst($this->accountId);
    }

    protected $value;
    public function getValue()
    {
        return $this->value;
    }

    protected $currency;
    public function getCurrency()
    {
        return $this->currency;
    }

    protected $type;
    public function getType()
    {
        return $this->type;
    }

    protected $sub_type;
    public function getSubType()
    {
        return $this->sub_type;
    }

    protected $counterparty_iban;
    public function getCounterpartyIban()
    {
        if($this->counterparty_iban == null)
        {
            return false;
        }
        return $this->counterparty_iban;
    }

    protected $counterparty_name;
    public function getCounterpartyName()
    {
        return $this->counterparty_name;
    }

    protected $counterparty_id;
    public function getCounterpartyAccount()
    {
        if($this->counterparty_id == null)
        {
            return false;
        }
        return Account::findFirst($this->counterparty_id);
    }

    protected $createdAt;
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    protected $modifiedAt;
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    protected $firefly_id;
    public function getFireflyId()
    {
        return $this->firefly_id;
    }

    public function getFireflyType()
    {
        // withdrawal (ausgaben)
        // deposit (einnahmen / einkommen)
        // transfers
        switch ($this->getSubType())
        {
            case 'payment':
                switch ($this->getType())
                {
                    case 'mastercard':
                        return 'withdrawal';
                    break;
                    
                    default:
                        
                    break;
                }
            break;
            
            default:
                # code...
                break;
        }
    }

    public function beforeCreate()
    {
        $this->createdAt = time();
    }

    public function beforeSave()
    {
        if($this->createdAt == null)
        {
            $this->createdAt = time();
        }
        $this->modifiedAt = time();
    }


    private function createFireflyTransaction()
    {
        $client = new FireflyClient();
        $data = array(
            'type' => $this->getFireflyType(),
            'date' => date("Y-m-d\TH:i:sP",$this->createdAt())
        );
        $request = $client->post('v1/transactions',array('transactions' => $data));

        if(!$request)
        {
            return false;
        }

        var_dump($request['data']);

        if(isset($request['data']))
        {
            $this->firefly_id = $request['data']['id'];
        }
        return true;
    }
}