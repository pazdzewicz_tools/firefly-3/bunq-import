<?php
use Phalcon\Cli\Task;

use bunq\Context\ApiContext;
use bunq\Context\BunqContext;
use bunq\Util\BunqEnumApiEnvironmentType;
use bunq\Model\Generated\Endpoint\MonetaryAccount;
use bunq\Model\Generated\Endpoint\Payment;

use Pazdzewicz\Models\Account;
use Pazdzewicz\Models\Transaction;

class ImportTask extends Task
{
  public function mainAction()
  {
    $apiContext = ApiContext::create(
        BunqEnumApiEnvironmentType::PRODUCTION(),
        $this->config->bunq->apikey,
        'My First PHP bunq Implementation!'
    );
    
    BunqContext::loadApiContext($apiContext);
    $monetaryAccountListing = MonetaryAccount::listing();
    $monetaryAccounts = $monetaryAccountListing->getValue();
    foreach($monetaryAccounts as $account)
    {
        if($account->getMonetaryAccountBank() != NULL)
        {
          $this->createMonetaryAccount($account->getMonetaryAccountBank());
          $this->getTransactions($account->getMonetaryAccountBank());
        }

        if($account->getMonetaryAccountJoint() != NULL)
        {
          $this->createMonetaryAccount($account->getMonetaryAccountJoint(),'joint');
          $this->getTransactions($account->getMonetaryAccountJoint());
        }

        if($account->getMonetaryAccountSavings() != NULL)
        {
          $this->createMonetaryAccount($account->getMonetaryAccountSavings(),'savings');
          $this->getTransactions($account->getMonetaryAccountSavings());
        }

        # NOT YET PRODUCTIVE IN BUNQ SDK, HOWEVER IN API
        #if($account->getMonetaryAccountInvestment() != NULL)
        #{
        #  $this->createMonetaryAccount($account->getMonetaryAccountInvestment(),'investment');
        #}
    }
  }


  private function getTransactions($monetaryAccountBank)
  {
    if(strtoupper($monetaryAccountBank->getStatus()) == strtoupper('cancelled'))
    {
      return true;
    }
    #var_dump($monetaryAccountBank);
    $payments = Payment::listing($monetaryAccountBank->getId());
    #$this->getDi()->get('logger')->error(json_encode($payments->getValue()));

    foreach ($payments->getValue() as $payment)
    {
      $this->createTransaction($payment);
    }
  }

  private function createTransaction($payment)
  {
    #var_dump($payment);
    $transaction = new Transaction([
      'id' => $payment->getId(),
      'accountId' => $payment->getMonetaryAccountId(),
      'amount' => $payment->getAmount()->getValue(),
      'counterparty_iban' => $payment->getCounterpartyAlias()->getIban(),
      'counterparty_name' => $payment->getCounterpartyAlias()->getDisplayName(),
      'createdAt' => strtotime($payment->getCreated()),
      'type' => strtolower($payment->getType()),
      'sub_type' => strtolower($payment->getSubType()),
      'currency' => $payment->getAmount()->getCurrency(),
      'description' => $payment->getDescription()
    ]);

    if($transaction->getCounterpartyIban())
    {
      $internal_account = Account::findFirst([
        'iban LIKE :iban:',
        'bind' => [
          'iban' => $transaction->getCounterpartyIban()
        ]
      ]);

      if($internal_account)
      {
        $transaction->assign([
          'counterparty_id' => $internal_account->getId()
        ]);
      }
    }

    if(!$transaction->create())
    {
      if($transaction->getMessages())
      {
        foreach ($transaction->getMessages() as $message)
        {
          $this->getDi()->get('logger')->error($message->getMessage().' - '.json_encode($transaction->toArray()));
        }
      }
      return false;
    }
  }

  private function createMonetaryAccount($monetaryAccountBank,$type = 'single')
  {
    if(strtoupper($monetaryAccountBank->getStatus()) == strtoupper("CANCELLED"))
    {
      // Dont import cancelled accounts
      return true;
    }

    $iban = '';
    $name = '';
    foreach ($monetaryAccountBank->getAlias() as $alias)
    {
      if($alias->getType() == 'IBAN')
      {
        $iban = $alias->getValue();
        $name = $alias->getName();
      }
    }

    $account = new Account([
      'id' => $monetaryAccountBank->getId(),
      'iban' => $iban,
      'owner' => $name,
      'currency' => $monetaryAccountBank->getCurrency(),
      'name' => $monetaryAccountBank->getDescription(),
      'opening_balance' => $monetaryAccountBank->getBalance()->getValue(),
      'opening_balance_date' => time(),
      'type' => $type,
      'status' => $monetaryAccountBank->getStatus(),
      'createdAt' => time()
    ]);


    if(!$account->create())
    {
      if($account->getMessages())
      {
        foreach ($account->getMessages() as $message)
        {
          $this->getDi()->get('logger')->error($message->getMessage().' - '.json_encode($account->toArray()));
        }
      }
      return false;
    }
    return true;
  }
}
