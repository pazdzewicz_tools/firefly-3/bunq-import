<?php

use Phalcon\Loader;

$loader = new Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerNamespaces([
    'Pazdzewicz\Models'      => $config->application->modelsDir,
    'Pazdzewicz\Controllers' => $config->application->controllersDir,
    'Pazdzewicz\Forms'       => $config->application->formsDir,
    'Pazdzewicz'             => $config->application->libraryDir
]);

$loader->registerDirs([
    $config->application->tasksDir,
]);

$loader->register();

// Use composer autoloader to load vendor classes
require_once BASE_PATH . '/vendor/autoload.php';
