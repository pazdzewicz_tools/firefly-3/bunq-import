<?php
/*
 * Modified: prepend directory path of current file, because of this file own different ENV under between Apache and command line.
 * NOTE: please remove this comment.
 */
defined('BASE_PATH') || define('BASE_PATH', getenv('BASE_PATH') ?: realpath(dirname(__FILE__) . '/../..'));
defined('APP_PATH') || define('APP_PATH', BASE_PATH . '/app');

use Phalcon\Logger;

return new \Phalcon\Config([
    'database' => [
        'adapter'     => 'Mysql',
        'host'        => getenv('MYSQL_HOST'),
        'username'    => getenv('MYSQL_USER'),
        'password'    => getenv('MYSQL_PASSWORD'),
        'dbname'      => getenv('MYSQL_DATABASE'),
        'unix_socket' => getenv('MYSQL_UNIX_SOCKET')?getenv('MYSQL_UNIX_SOCKET'):false,
        'charset'     => 'utf8',
    ],
    'application' => [
        'appDir'         => APP_PATH . '/',
        'controllersDir' => APP_PATH . '/controllers/',
        'modelsDir'      => APP_PATH . '/models/',
        'migrationsDir'  => APP_PATH . '/migrations/',
        'viewsDir'       => APP_PATH . '/views/',
        'pluginsDir'     => APP_PATH . '/plugins/',
        'libraryDir'     => APP_PATH . '/library/',
        'cacheDir'       => BASE_PATH . '/cache/',
        'formsDir'       => APP_PATH . '/forms/',
        'tasksDir'       => APP_PATH . '/tasks/',
        'baseUri'        => '/',
    ],
    'logger' => [
        'path'     => BASE_PATH . '/logs/',
        'format'   => '%date% [%type%] %message%',
        'date'     => 'D j H:i:s',
        'logLevel' => Logger::DEBUG,
        'filename' => 'application.log'
    ],
    'bunq' => [
        'apikey' => getenv('BUNQ_API_KEY'),
        'env' => getenv('BUNQ_ENV'),
    ],
    'firefly' => [
        'apikey' => getenv('FIREFLY_API_KEY'),
        'url' => getenv('FIREFLY_API_URL'),
    ]
]);
