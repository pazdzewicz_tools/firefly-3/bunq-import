<?php

use Phalcon\Mvc\Router;
use Phalcon\Mvc\Dispatcher;

use Phalcon\Mvc\Url as UrlResolver;

use Phalcon\Db\Adapter\Pdo\MySQL as MySQLAdapter;

// Logger
use Phalcon\Logger\Adapter\File as FileLogger;
use Phalcon\Logger\Formatter\Line as FormatterLine;
use Phalcon\Cache\Frontend\Data as FrontData;


/**
 * Shared configuration service
 */
$di->setShared('config', function () {
    $config = include APP_PATH . '/config/config.php';

    if (is_readable(APP_PATH . '/config/config.dev.php')) {
        $override = include APP_PATH . '/config/config.dev.php';
        $config->merge($override);
    }

    return $config;
});

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () {
    $config = $this->getConfig();

    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->setShared('db', function () {
    $config = $this->getConfig();

    $params = [
        'host' => $config->database->host,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname' => $config->database->dbname,
        'charset' => $config->database->charset
    ];

    if($config->database->unix_socket != false)
    {
        unset($params['host']);
        unset($params['dbname']);
        unset($params['charset']);
        $params['dsn'] = sprintf(
            'mysql:unix_socket=%s;dbname=%s;',
            $config->database->unix_socket,
            $config->database->dbname
        );
    }
    $connection = new MySQLAdapter($params);
    return $connection;
});

/**
 * Generic Logger service
 */
$di->set('logger', function ($filename = null, $format = null) {
    $config = $this->getConfig();
    $format   = $format ?: $config->get('logger')->format;
    $filename = trim($filename ?: $config->get('logger')->filename, '\\/');
    $path     = rtrim($config->get('logger')->path, '\\/') . DIRECTORY_SEPARATOR;
    $formatter = new FormatterLine($format, $config->get('logger')->date);
    $logger    = new FileLogger($path . $filename);
    $logger->setFormatter($formatter);
    $logger->setLogLevel($config->get('logger')->logLevel);
    return $logger;
});
