<?php

use Phalcon\Di\FactoryDefault\Cli as CliDI;
use Phalcon\Cli\Console as ConsoleApp;
use Phalcon\Loader;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;

/**
 * Define some useful constants
 */
define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app');

// Using the CLI factory default services container
$di = new CliDI();


/**
 * Read services
 */
include APP_PATH . '/config/services-cli.php';

/**
 * Get config service for use in inline setup below
 */
$config = $di->getConfig();

/**
 * Include Autoloader
 */
include APP_PATH . '/config/loader.php';

// Use composer autoloader to load vendor classes
require_once BASE_PATH . '/vendor/autoload.php';

// Create a console application
$console = new ConsoleApp();

$console->setDI($di);

/**
 * Process the console arguments
 */
$cliarguments = [];

foreach ($argv as $k => $arg) {
    if ($k === 1) {
        $cliarguments['task'] = $arg;
    } elseif ($k === 2) {
        $cliarguments['action'] = $arg;
    } elseif ($k >= 3) {
        $cliarguments['params'][] = $arg;
    }
}

try {
    // Handle incoming arguments
    $console->handle($cliarguments);
} catch (\Phalcon\Exception $e) {
    // Do Phalcon related stuff here
    // ..
    fwrite(STDERR, $e->getMessage() . PHP_EOL);
    exit(1);
} catch (\Throwable $throwable) {
    fwrite(STDERR, $throwable->getMessage() . PHP_EOL);
    exit(1);
} catch (\Exception $exception) {
    fwrite(STDERR, $exception->getMessage() . PHP_EOL);
    exit(1);
}

