<?php
namespace Pazdzewicz\Firefly;

use Phalcon\Mvc\User\Component;

class Client extends Component
{
  public function post($url,$data)
  {
    return $this->curl($url,$data,"post");
  }

  public function put($url,$data)
  {
    return $this->curl($url,$data,"put");
  }

  public function delete($url,$data = null)
  {
    return $this->curl($url,$data,"delete");
  }

  public function get($url)
  {
    return $this->curl($url,null,"get");
  }

  private function curl($url,$data = null,$request)
  {
    // URL
    $url = $this->config->firefly->url.'/api/'.$url;
    // Options
    $options = array(
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_FAILONERROR => false,
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json',
        'accept: application/vnd.api+json',
        'Authorization: Bearer '.$this->config->firefly->apikey
      ),
    );
    switch ($request)
    {
      case 'post':
        $options[CURLOPT_POST]=true;
        if($data)
        {
          if(is_array($data))
          {
            $options[CURLOPT_POSTFIELDS]=json_encode($data);
          }
          else
          {
            $options[CURLOPT_POSTFIELDS]=$data;
          }
        }
      break;

      case 'put':
        $options[CURLOPT_CUSTOMREQUEST]="PUT";
        if($data)
        {
          if(is_array($data))
          {
            $options[CURLOPT_POSTFIELDS]=json_encode($data);
          }
          else
          {
            $options[CURLOPT_POSTFIELDS]=$data;
          }
        }
      break;

      case 'delete':
        $options[CURLOPT_CUSTOMREQUEST]="DELETE";
        if($data)
        {
          if(is_array($data))
          {
            $options[CURLOPT_POSTFIELDS]=json_encode($data);
          }
          else
          {
            $options[CURLOPT_POSTFIELDS]=$data;
          }
        }
      break;
    }
    switch ($request)
    {
      case 'post':
      case 'put':
        $this->getDi()->get('logger')->info("Request ".$request." ".$url." ".json_encode($options));
      break;

      default:
        $this->getDi()->get('logger')->info("Request ".$request." ".$url);
      break;
    }
    // Start CURL
    $curl = curl_init($url);
    curl_setopt_array($curl, $options);
    $content = curl_exec($curl);
    $response_code=curl_getinfo($curl,CURLINFO_RESPONSE_CODE);
    curl_close($curl);
    $this->getDi()->get('logger')->info("Response ".$response_code." ".$request." ".$url." ".$content);
    if(!$content)
    {
      return false;
    }
    return json_decode($content,true);
  }
}
